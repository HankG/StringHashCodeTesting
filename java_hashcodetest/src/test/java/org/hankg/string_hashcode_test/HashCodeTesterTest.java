package org.hankg.string_hashcode_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Map;

import org.junit.Test;

public class HashCodeTesterTest {

	@Test
	public void testHashCodeTester() {
		assertNotNull(new HashCodeTester());
	}

	@Test
	public void testGetCount() {
		String[] ss = new String[] {"Hello", "World", "Hello"};
		int expectedCount = 3;
		HashCodeTester tester = new HashCodeTester();
		assertEquals(0, tester.getCount());
		int index = 0;
		for(String s : ss) {
			tester.integrateString(s, index++);
		}
		assertEquals(expectedCount, tester.getCount());
	}

	@Test
	public void testGetDuplicates() {
		String[] ss = new String[] {"Hello", "World", "Hello", "Aa", "BB", "Hello"};
		int expectedDuplicateHashes = 2;
		int expectedHelloDuplicates = 3;
		int expectedBadDuplicates = 2;
		HashCodeTester tester = new HashCodeTester();
		int index = 0;
		for(String s : ss) {
			tester.integrateString(s, index++);
		}
		
		Map<Integer, List<Long>> duplicates = tester.getDuplicates();
		assertEquals(expectedDuplicateHashes, duplicates.size());
		
		List<Long> ds = duplicates.get(ss[0].hashCode());
		assertEquals(expectedHelloDuplicates, ds.size());
		assertEquals(0L, ds.get(0).longValue());
		assertEquals(2L, ds.get(1).longValue());
		assertEquals(5L, ds.get(2).longValue());
		
		ds = duplicates.get(ss[3].hashCode());
		assertEquals(expectedBadDuplicates, ds.size());
		assertEquals(3L, ds.get(0).longValue());
		assertEquals(4L, ds.get(1).longValue());
	}

	@Test
	public void testGetMaximumLength() {
		String s1 = "a";
		String s2 = "an";
		String s3 = "the";
		int expectedMax = 3;
		HashCodeTester tester = new HashCodeTester();
		assertEquals(0, tester.getMaximumLength());
		tester.integrateString(s1, 0);
		tester.integrateString(s2, 1);
		tester.integrateString(s3, 2);
		assertEquals(expectedMax, tester.getMaximumLength());
	}

	@Test
	public void testGetMinimumLength() {
		String s1 = "a";
		String s2 = "an";
		String s3 = "the";
		int expectedMin = 1;
		HashCodeTester tester = new HashCodeTester();
		assertEquals(0, tester.getMaximumLength());
		tester.integrateString(s1, 0);
		tester.integrateString(s2, 1);
		tester.integrateString(s3, 2);
		assertEquals(expectedMin, tester.getMinimumLength());
	}

	@Test
	public void testIntegrateString() {
		String s1 = "a";
		String s2 = "an";
		String s3 = "the";
		HashCodeTester tester = new HashCodeTester();
		tester.integrateString(s1, 0);
		tester.integrateString(s2, 1);
		tester.integrateString(s3, 2);
	}

}
