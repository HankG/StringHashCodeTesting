package org.hankg.string_hashcode_test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Class which tests and aggregates the hash code analysis
 *
 */
public class HashCodeTester {
	private Map<Integer, Long> allHashs;
	private Map<Integer, List<Long>> duplicates;
	private Object duplicateListLocker;
	private AtomicLong sizeMax;
	private AtomicLong sizeMin;
	private AtomicLong sizeSum;
	private AtomicLong sizeCount;
	
	public HashCodeTester() {
		this.allHashs = new ConcurrentHashMap<Integer, Long>();
		this.duplicates = new ConcurrentHashMap<Integer, List<Long>>();
		this.duplicateListLocker = new Object();
		this.sizeMax = new AtomicLong();
		this.sizeMin = new AtomicLong(Long.MAX_VALUE);
		this.sizeSum = new AtomicLong();
		this.sizeCount = new AtomicLong();
	}
	
	public long getCount() {
		return this.sizeCount.get();
	}
	
	public Map<Integer, List<Long>> getDuplicates() {
		return Collections.unmodifiableMap(this.duplicates);
	}
	
	public long getMaximumLength() {
		return this.sizeMax.get();
	}
		
	public long getMinimumLength() {
		return this.sizeMin.get();
	}
	
	public long getTotalSize() {
		return this.sizeSum.get();
	}
	
	public boolean integrateString(String s, long index) {
		boolean duplicate = false;
		long length = s.length();
		int h = s.hashCode();
		
		sizeCount.incrementAndGet();
		sizeSum.addAndGet(length);
		if(length > sizeMax.get()) {
			sizeMax.set(length);
		}
		
		if(length < sizeMin.get()) {
			sizeMin.set(length);
		}
		
		Long lastIndex = this.allHashs.putIfAbsent(h, index);
		if( lastIndex != null) {
			synchronized (duplicateListLocker) {
				List<Long> currentDuplicates = this.duplicates.get(h);
				if(currentDuplicates == null) {
					currentDuplicates = new ArrayList<Long>();
					currentDuplicates.add(lastIndex);
					this.duplicates.put(h, currentDuplicates);
				}
				
				currentDuplicates.add(index);
				duplicate = true;
			}
		}
		
		return duplicate;
	}

}
