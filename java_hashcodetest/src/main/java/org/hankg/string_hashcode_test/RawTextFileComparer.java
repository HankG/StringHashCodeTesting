package org.hankg.string_hashcode_test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

public class RawTextFileComparer {
	private static final ConcurrentHashMap<Long, ValueHolder> duplicateValues = new ConcurrentHashMap<Long, ValueHolder>();
	private static final ConcurrentHashMap<Long, String> allPaths = new ConcurrentHashMap<Long, String>();
	private static final int echoIncrement = 10000;
	private static final AtomicLong itemCounter = new AtomicLong();
	private static final StringBuffer outputCurve = new StringBuffer();
	private static final HashCodeTester tester = new HashCodeTester();
	private static final AtomicLong totalDuplicates = new AtomicLong();
	
	
	public static void main(String args[]) throws IOException {
		if(args.length != 1) {
			System.out.println("Usage java -jar RawTextFileComparer <rootFolderPath>");
		}
		
		Files.walkFileTree(Paths.get(args[0]),new SimpleFileVisitor<Path>() {
	         @Override
	         public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
	             throws IOException
	         {
	             if(attrs.isRegularFile()) {
	            	 long currentItem = itemCounter.getAndIncrement();
	            	 allPaths.put(currentItem, file.toString());
	            	 if(currentItem % echoIncrement == 0) {
	            		 System.out.println(currentItem + "->" + file.toString());
	            	 }
	            	 boolean successfulParsing = true;
	            	 StringBuffer sb = new StringBuffer();
	            	 try(Stream<String> stringStream = Files.lines(file)) {
	            		 stringStream.forEach((String s) -> sb.append(s));
	            	 } catch (Exception e) {
	            		 sb.delete(0, sb.length());
	            		 try(Stream<String> stringStream2 = Files.lines(file, Charset.forName("ISO-8859-1"))) {
	            			 stringStream2.forEach((String s) -> sb.append(s));
	            		 } catch(Exception e2) {
		            		 System.err.println(currentItem + " -> " + file.toString());
		            		 e2.printStackTrace();
	            		 }
	            	 }

	            	 if(successfulParsing && tester.integrateString(sb.toString(), currentItem)) {
	            		 System.out.println("Duplicate hash found: " + file.toString());
	            		 outputCurve.append(currentItem + "," + totalDuplicates.incrementAndGet() + System.lineSeparator());
	            		 duplicateValues.put(currentItem, new ValueHolder(sb.toString(), file.toString(), currentItem));
	            	 }
	             }
	             return FileVisitResult.CONTINUE;
	         }
		});
		
		System.out.println("Done processing...duplicates: " + duplicateValues.size());
		System.out.println("Total Count: " + tester.getCount());
		System.out.println("Minimum Size: " + tester.getMinimumLength());
		System.out.println("Max Size: " + tester.getMaximumLength());
		System.out.println("Average size: " + (double)tester.getTotalSize() / tester.getCount());
		System.out.println("Duplicate curve: " );
		System.out.println(outputCurve);
		
		System.out.println("Duplicate hashes: " );
		for(int duplicateHash : tester.getDuplicates().keySet()) {
			List<Long> duplicates = tester.getDuplicates().get(duplicateHash);
			System.out.println(duplicateHash + " : " + duplicates.toString());
			for(int index = 0 ; index < duplicates.size(); index++) {
				System.out.println(index + "->" + allPaths.get(duplicates.get(index)));
			}
		}
		
		System.out.println("Duplicate lines: ");
		for(long l : duplicateValues.keySet()) {
			System.out.println(duplicateValues.get(l).toString());
		}
	}
	
}
