package org.hankg.string_hashcode_test;

public class ValueHolder {
	private final String fieldValue;
	private final String fileName;
	private final long index;
	
	@SuppressWarnings("unused")
	private ValueHolder() {
		this("","",0);
	}
	
	ValueHolder(String fileName, String fieldValue, long index) {
		this.fieldValue = fieldValue;
		this.fileName = fileName;
		this.index = index;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public String getFileName() {
		return fileName;
	}

	public long getIndex() {
		return index;
	}

	@Override
	public String toString() {
		return "ValueHolder [fieldValue=" + fieldValue + ", fileName=" + fileName + ", index=" + index + "]";
	}
	
	
}
